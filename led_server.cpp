#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <pthread.h>
#include <errno.h>
#include <signal.h>


#include <iostream>

#include "led_controller.h"
 
 using namespace std;
 
static const char* read_pipe_name = "/tmp/led_server_read";
static const char* write_pipe_name = "/tmp/led_server_write";

void server_loop(led_controller& led_ctr);
int pipe_write(const char* data, size_t sz);


void signal_callback_handler(int signum)
{
    printf("Caught signal %d\n",signum);
   
	int ret_code = 0;
	ret_code += remove(read_pipe_name);
	ret_code += remove(write_pipe_name);
	if (ret_code != 0) {
		cerr << "Pipes are not exist" << endl;
	}

   exit(signum);
}

int main()
{
	//Register signals and signal handler
	signal(SIGINT, signal_callback_handler);
	signal(SIGKILL, signal_callback_handler);
	signal(SIGSTOP, signal_callback_handler);

	//Create couple of fifos for read and write
	int fifo_r = mkfifo(read_pipe_name, 0666);
	int fifo_w = mkfifo(write_pipe_name, 0666);
	if(fifo_r == -1 || fifo_w == -1)
	{
		if(errno == EACCES)
		{
			cerr << "permission denied" << endl;
			return 1;
		}
	}
	
	//Led
    led_controller led_ctr;
    
    //Run R/W loop
	server_loop(led_ctr);
 
	return 0;
}

int pipe_write(const char* data, size_t size)
{
	int fd = open(write_pipe_name, O_WRONLY);
	if(fd < 0)
		return 1;
		
	write(fd, data, size);
	close(fd);
	
	return 0;
}
 
void server_loop(led_controller& led_ctr)
{
	while (1) {
		int fd;
		size_t len;
		size_t tally = 0;
		fd = open(read_pipe_name, O_RDONLY);
		char read_buf[PIPE_BUF];
		memset((void*)read_buf, 0, PIPE_BUF);
		while ((len = read(fd, read_buf, PIPE_BUF)) > 0) tally += len;
		
		//Process command
		//printf("Read : %s",read_buf);
		string ans = led_ctr.execute(read_buf);
		
		pipe_write(ans.c_str(),ans.size());
		close(fd);
	}
}
