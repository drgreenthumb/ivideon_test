#!/bin/bash

read_pipe=/tmp/led_server_write
write_pipe=/tmp/led_server_read

#Check, thet pipes exist
if [ ! -p $read_pipe ] || [ ! -p $write_pipe ]; then
    echo "FAILED (Server not running)"
    exit 1
fi

#Get client pid
client_id=$BASHPID 

#R/W loop
while true
do

	#Write 
    echo '>'
	read from_tty </dev/tty
    
    #Build request string	
	in_data=${from_tty#* }
	in_data=${in_data% *}
	in_command=${from_tty% *} 
	arg="$client_id:$in_command[$in_data]"
 	#echo $arg
 	
 	#Send
    echo $arg >$write_pipe
    
    #Receive
	read ans <$read_pipe 
	echo $ans

	#Parse, example : 3478:OK[on]
	received_id=${ans%:*}
	if [ $client_id == $received_id ]; then
    	out=${ans#*:}
    	echo "$out" | tr '[' ' ' | tr ']' ' '
	fi
	
done
