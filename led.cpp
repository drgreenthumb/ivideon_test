#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include <iostream>

#include "led.h"
#include "led_controller.h"

//define colour names
#define RED_NAME   "red"
#define GREEN_NAME "green"
#define BLUE_NAME  "blue"

#define TIME_IND_BUF_SIZE (128)

typedef enum _led_colour_t {none,red,green,blue} led_colour_t;

using namespace std;

int LED::check_and_set_colour(string colour)
{	
	if(strcmp(colour.c_str(), RED_NAME) == 0)
	{
		controls.colour = RED_NAME;
		return 0;
	}
	else if(strcmp(colour.c_str(), GREEN_NAME) == 0)
	{
		controls.colour = GREEN_NAME;
		return 0;
	}
	else if(strcmp(colour.c_str(), BLUE_NAME) == 0)
	{
		controls.colour = BLUE_NAME;
		return 0;
	}
	
	return 1;
}

void* led_loop(void *p)
{
    LED::led_controls* ctr_ptr = (LED::led_controls*)p;

	while(1)
	{	
		//Led state indication
		sleep(1);
		time_t rawtime;
		struct tm * timeinfo;
  		char time_buf[TIME_IND_BUF_SIZE];
  		time (&rawtime);
  		timeinfo = localtime (&rawtime);
		strftime (time_buf,TIME_IND_BUF_SIZE,"%H:%M:%S",timeinfo);

		cout << time_buf << " : " << "state=" << ctr_ptr->state << " : " << 
			"colour=" << ctr_ptr->colour << " : " << 
			"rate=" << ctr_ptr->rate << endl;
	}
	
	return NULL;
}

LED::LED() 
{
	controls.state = false;
	controls.rate = 0;
	controls.colour = RED_NAME;
		
	pthread_create(&led_pid, 0, led_loop, (void*)&controls);
}

LED::~LED()
{
	pthread_cancel(led_pid);
}

int LED::set_state(bool state)
{
	this->controls.state = state;
	return 0;
}

int LED::set_rate(float rate)
{
    if(rate >= min_rate && rate <= max_rate)
		this->controls.rate = rate;
	else
		return 1;
		
	return 0;
}

std::string LED::set_color(std::string colour)
{
	if(check_and_set_colour(colour))
		return STATE_FAIL;
		
	return STATE_OK;
}

bool LED::get_state()
{
	return controls.state;
}
	
float LED::get_rate()
{
	return controls.rate;
}
	
string LED::get_colour()
{
	return controls.colour;
}
