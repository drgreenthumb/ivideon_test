#include <stdlib.h> 
#include <stdio.h>
#include <string.h>

#include <iostream>
#include <sstream> 

#include "led_controller.h"

using namespace std;


int led_controller::led_controller_init()
{
	//Initialize commands map
	commands["set-led-state"] = new set_led_state;
    commands["set-led-color"] = new set_led_color;
    commands["set-led-rate"] = new set_led_rate;
    commands["get-led-state"] = new get_led_state;
    commands["get-led-color"] = new get_led_color;
    commands["get-led-rate"] = new get_led_rate;
    
    return 0;
}

int led_controller::led_controller_deinti()
{
	for (std::map<string, i_led_command*>::iterator it = commands.begin(); 
		it!=commands.end(); 
		++it)
		delete it->second;
		
	return 0;
}

led_controller::led_controller()
{
	try 
	{
		led_controller_init();
	}
	catch(...)
    {
    	cerr << "Exception : " << __FILE__ << " : " << __LINE__ << endl;
    }
}

led_controller::~led_controller(){
	
	try 
	{
		led_controller_deinti();
	}
	catch(...)
    {
    	cerr << "Exception : " << __FILE__ << " : " << __LINE__ << endl;
    }
}

std::string led_controller::execute(std::string message)
{
	//Parse message, example : 7718:command[argument]
	string pid = message.substr(0,message.find(":"));
	
	string argument = message.substr(message.find("[") + 1);
	argument[argument.length() - 2] = 0;
	
	string command = message.substr(
		message.find(":") + 1,
		message.size() - pid.size() - argument.size() - 2
		);
	
	//Process command
	i_led_command* c_ptr = commands[command];
	if(c_ptr == 0)
		return STATE_FAIL;
		
	string command_result = c_ptr->execute(argument, led);
	
	stringstream ss;
	ss << pid << ":" << command_result;
	
	return ss.str();
}

//Strategy implementations
string set_led_state::execute(std::string argument, LED& led)
{
	if(strcmp(argument.c_str(), "on") == 0)
		led.set_state(true);
	else if(strcmp(argument.c_str(), "off") == 0)
		led.set_state(false);
	else
		return STATE_FAIL;
		
	return STATE_OK;
}

string set_led_rate::execute(std::string argument, LED& led)
{
	float rate = atof(argument.c_str());
	if(led.set_rate(rate))
		return STATE_FAIL;
	else
		return STATE_OK;
}

string set_led_color::execute(std::string argument, LED& led)
{
	return led.set_color(argument);
}

string get_led_state::execute(std::string argument, LED& led)
{
	bool state = led.get_state();
	stringstream ss;
	if(state == true)
	{
		ss << STATE_OK << "[on]";
		return ss.str();
	}
	else 
	{
		ss << STATE_OK << "[off]";
		return ss.str();
	}
}

string get_led_color::execute(std::string argument, LED& led)
{
    stringstream ss;
	ss << STATE_OK << "[" << led.get_colour() << "]";
	return ss.str();
}

string get_led_rate ::execute(std::string argument, LED& led)
{
	stringstream ss;
	ss << STATE_OK << "[" << led.get_rate() << "]"; 
	return ss.str();
}

