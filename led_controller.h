#pragma once

#include <map>
#include <string>

#include "led.h"

#define STATE_OK "OK"
#define STATE_FAIL "FAILD"

class i_led_command;

class led_controller
{
	LED led;
	std::map<std::string, i_led_command*> commands;
	
	int led_controller_init();
	int led_controller_deinti();
	
public:
	led_controller();
	~led_controller();
		
	std::string execute(std::string message);
};

//Strategy interface
class i_led_command
{
public:
	virtual std::string execute(std::string command, LED& led) = 0;
	virtual ~i_led_command(){}
};

//Implementations
class set_led_state : public i_led_command
{
public:
	virtual std::string execute(std::string argument, LED& led);
};

class set_led_color : public i_led_command
{
public:
	virtual std::string execute(std::string argument, LED& led);
};

class set_led_rate : public i_led_command
{
public:
	virtual std::string execute(std::string argument, LED& led);
};

class get_led_state : public i_led_command
{
public:
	virtual std::string execute(std::string argument, LED& led);
};

class get_led_color : public i_led_command
{
public:
	virtual std::string execute(std::string argument, LED& led);
};

class get_led_rate : public i_led_command
{
public:
	virtual std::string execute(std::string argument, LED& led);
};

