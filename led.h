#pragma once

#include <pthread.h>
#include <string>

class LED
{
public:
	typedef struct 
	{
		bool state;
		float rate;
		std::string colour;
	} led_controls;
	
private:
	static const int min_rate = 0;
	static const int max_rate = 5;
	
	led_controls controls;
	pthread_t led_pid;
	
	
	int check_and_set_colour(std::string colour);

public:

	LED();
	~LED();

	int set_state(bool state);
	int set_rate(float rate);
	std::string set_color(std::string colour);

	bool get_state();
	float get_rate();
	std::string get_colour();
};


