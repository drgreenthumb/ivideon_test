all:
	g++ -pthread -Wall -I. -o led_server led_server.cpp led_controller.cpp led.cpp

install:
	cp ./led_server /usr/bin/

uninstall:
	rm /usr/bin/led_server
	
clean:
	rm ./led_server
